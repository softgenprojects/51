import { Box, VStack, Heading, Button, Text, Input, useColorModeValue, Center, Flex, SimpleGrid, Link, Image } from '@chakra-ui/react';
import { FaSearch, FaTwitter } from 'react-icons/fa';

const Home = () => {
  const bgColor = useColorModeValue('gray.50', 'gray.800');
  const textColor = useColorModeValue('gray.800', 'whiteAlpha.900');

  return (
    <Box bg={bgColor} color={textColor} minH="100vh" py="12" px={{ base: '4', lg: '8' }}>
      <Center mb="12">
        <Image src="https://www.hamstersniffer.com/static/images/hamster-sniffer-xl.png" alt="Hamster Sniffer Logo" />
      </Center>
      <VStack spacing="8" mb="20">
        <Heading as="h1" size="2xl" textAlign="center">
          Your Ultimate Weapon Against Token Scams
        </Heading>
        <Text textAlign="center" fontSize={{ base: 'md', md: 'lg', lg: 'xl' }}>
          Unmatched precision, speed, and earnings potential, all in one user-friendly tool.
        </Text>
        <Flex as="form" w="full" maxW="md" mx="auto">
          <Input placeholder="Search tokens by name or address..." mr="2" />
          <Button leftIcon={<FaSearch />} colorScheme="purple">
            Search
          </Button>
        </Flex>
      </VStack>
      <SimpleGrid columns={{ base: 1, md: 2, lg: 3 }} spacing="40px" mb="20">
        <Box shadow="xl" p="5" borderRadius="lg">
          <Heading size="md">Small accounts, Big earnings</Heading>
          <Text mt="4">Unlock the same earning potential as 10x larger accounts.</Text>
        </Box>
        <Box shadow="xl" p="5" borderRadius="lg">
          <Heading size="md">Superior Scam-to-Non-Scam Ratio</Heading>
          <Text mt="4">Protect your investments with our 90% hit rate on scam detection.</Text>
        </Box>
        <Box shadow="xl" p="5" borderRadius="lg">
          <Heading size="md">Detailed Token Analysis</Heading>
          <Text mt="4">Gain the competitive edge with our up-to-second insights.</Text>
        </Box>
      </SimpleGrid>
      <VStack spacing="8" mb="20">
        <Heading as="h2" size="xl" textAlign="center">
          Unleash the Power of HamsterSniffer
        </Heading>
        <Text textAlign="center" fontSize={{ base: 'md', md: 'lg', lg: 'xl' }}>
          Join over 500+ successful traders already hitting big with today’s best-rated API.
        </Text>
        <Button colorScheme="purple" size="lg">
          Coming Soon
        </Button>
      </VStack>
      <Center mb="12">
        <VStack spacing="4">
          <Input placeholder="Enter your email" />
          <Button colorScheme="purple" size="md">
            Subscribe
          </Button>
        </VStack>
      </Center>
      <Flex justify="space-between" px={{ base: '4', lg: '8' }} py="4" borderTopWidth="1px">
        <Text>© 2023 DeFi Venture Capital</Text>
        <Flex>
          <Link href="https://twitter.com" isExternal mr="4"><FaTwitter /></Link>
          <Link href="#">Terms of Use</Link>
          <Link href="#" ml="4">Privacy Policy</Link>
          <Link href="#" ml="4">Donations</Link>
        </Flex>
      </Flex>
    </Box>
  );
};

export default Home;